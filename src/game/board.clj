(ns game.board
  (:require [clojure.set :as set]))

(def enemy-presence-probability
  "Number to decide the probability of having an enemy at a cell on the board.
  A higher value implies greater difficulty."
  0.09)

(declare find-a-path)

(defn gen-board
  "Generate a game board with row-count rows and col-count columns.

  Rows and columns are zero-indexed. A cell is simply a list of two elements,
  i.e. the row and the column. Origin it at bottom left corner of the board
  with +x-axis going towards the right and +y-axis upwards from there.

  Meaning of fields in the returned map:
  | Field        | Description                           |
  |--------------+---------------------------------------|
  | :enemy-cells | set of cells that contain enemies     |
  | :beg-cell    | cell the user starts at               |
  | :exit-cell   | cell the user tries to get to         |
  | :rows, :cols | rows and columns in the board itself. |
  |--------------+---------------------------------------|
  "
  [row-count col-count]
  {:pre [(< 1 row-count) (< 1 col-count)]}
  (let [all-cells (for [i (range row-count)
                        j (range col-count)]
                    [i j])
        beg-cell [0, 0]
        exit-cell (->> all-cells (remove #{beg-cell}) rand-nth)
        a-random-path (find-a-path [row-count col-count]
                                   beg-cell
                                   exit-cell)
        enemy-cells (->> all-cells
                         (remove (set/union #{beg-cell exit-cell}
                                            (set a-random-path)))
                         (random-sample enemy-presence-probability)
                         set)]
    {:rows row-count
     :cols col-count
     :enemy-cells enemy-cells
     :beg-cell  beg-cell
     :exit-cell exit-cell
     ;; Useful for visualizing the open path, try and be amazed by the beauty:
     ;; (let [board (gen-board 10 10)]
     ;;    (print (render board user (set (:open-path board)))))
     :open-path a-random-path}))

(defn cell-neighbors
  "Given row and column numbers, compute the set neighbor cells on board."
  ([board-rows board-cols [row col]]
   (->> (for [delta-row [-1 0 1]
              delta-col [-1 0 1]
              :when (not= delta-row delta-col 0)]
          [(+ row delta-row) (+ col delta-col)])

        (filter #(and (< -1 (first %) board-rows)
                      (< -1 (second %1) board-cols)))

        set))
  ([board cell]
   (cell-neighbors (:rows board) (:cols board) cell)))

(defn render
  "Return a string representation of board along with user.
  show-cell? is a predicate that is called with [i j], the row and column numbers
  and if it returns false, [i j] is hidden from the user."
  [{:keys [rows cols enemy-cells exit-cell] :as board}
   {:keys [current-pos visited-cells points] :as user}
   show-cell?]
  (->> (for [i (range -1 (inc rows))
             j (range -1 (inc cols))]
         (cond
           ;; Boundaries
           (or (= i -1) (= j -1) (= i rows) (= j cols))              \.
           ;; Hide the cell
           (not (show-cell? [i j]))                                  \#
           ;; If exit cell is not hidden from user, show it
           (= [i j] exit-cell)                                       \*
           ;; User position
           (= [i j] current-pos)                                     \I
           ;; Enemy cells
           (contains? enemy-cells [i j])                             \e
           ;; Empty cell
           :else                                                     \space ))
       (partition (+ 2 cols))
       (reduce (fn [acc row-chars]
                 (format "%40s\n%s"
                         (apply str row-chars)
                         acc))
               "")
       (str "\nPoints: " points "\n")))


(defn- construct-path
  "Helper function used by `find-a-path`.
  Constructs a path from source to destination as sequence of values produced
  with prev-map, a map with value that tells us the previous node for a node."
  [prev-map source dest]
  (loop [path [dest]
         prev-node (get prev-map dest)]
    (if prev-node
      (recur (conj path prev-node) (get prev-map prev-node))
      (reverse path))))

(defn find-a-path
  "Finds a random path from source-cell to dest-cell on a board with board-dims.
  All arguments are pairs of integers. Producing all paths is very expensive as
  paths increase exponentially with product of board dimensions."
  ([board-dims source-cell dest-cell]
   (loop [stack (list source-cell)
          visited-cells #{source-cell}
          paths-map {}]
     (let [current-cell (peek stack)
           neighbors (-> (cell-neighbors (first board-dims)
                                         (second board-dims)
                                         current-cell)
                         (set/difference visited-cells)
                         shuffle)
           new-paths-map (reduce #(assoc %1 %2 current-cell)
                                 paths-map
                                 neighbors)]

       (if (= current-cell dest-cell)
         (construct-path paths-map source-cell dest-cell)
         (recur (into (pop stack) neighbors)
                (set/union visited-cells (set neighbors))
                new-paths-map))))))
