(ns game.core-rx
  (:require [rx.lang.clojure.core :as rx]
            [game.core :as gcore]
            [game.board :as board]
            [clojure.set :as set]))

(defn- game-state->progress-info
  "Return game info along with output string given game state."
  [{:keys [board user] :as game-state}]
  (cond
    (neg? (:points user))
    {:state :user-lost
     :output-string (str "=======GAME OVER=======\n"
                         (board/render board user (constantly true)))}

    (= (:current-pos user) (:exit-cell board))
    {:state :user-won
     :output-string (str "========YOU WON========\n"
                         (board/render board user (constantly true)))}

    :else
    {:state :in-progress
     :output-string (->> (board/cell-neighbors board (:current-pos user))
                         (set/union #{(:current-pos user)})
                         (board/render board user))}))

(defn make-user-commands-observable
  "Returns an Observable that emits user commands."
  []
  (rx/generator [observer]
                (loop []
                  (when-not (rx/unsubscribed? observer)
                    (rx/on-next observer (gcore/read-command))
                    (recur)))))

(defn make-game-state-observable
  "Given board size and user initial points, returns an Observable that emits
  game state on every user input."
  [row-count col-count initial-points]
  (let [board (board/gen-board row-count col-count)
        user (gcore/make-user board initial-points)
        initial-state {:board board
                       :user user}]
    (->> (make-user-commands-observable)
         (rx/reductions (fn [{:keys [board user] :as state} user-command]
                          (gcore/exec-user-command board user user-command))
                        initial-state)
         (rx.lang.clojure.core/merge (rx/return initial-state)))))

(defn start-game [row-count col-count initial-points]
  "Start the game by plumbling all observables."
  (-> (make-game-state-observable row-count col-count initial-points)
      (rx/subscribe (fn [game-state]
                      (let [{:keys [output-string state]}
                            (game-state->progress-info game-state)]
                        (println output-string)
                        (flush)
                        (when (not= state :in-progress)
                          (System/exit 0)))))))

(defn -main []
  (start-game 10 10 100))
