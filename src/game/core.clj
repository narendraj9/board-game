(ns game.core
  (:gen-class)
  (:require [clojure.set :as set]
            [game.board  :refer :all]))

(def point-loss-rate-in-combat
  "Factor by which each enemy around user reduces his/her points."
  10)

(def input-string->command
  "Map from input string to navigation/show-map command key."
  {"j"    :down
   "k"    :up
   "h"    :left
   "l"    :right
   "/"    :south-west
   "\\"   :south-east
   "`"    :north-west
   "'"    :north-east
   "m"    :show-map
   "?"    :show-help})

(def command-help
  "String for help with commands."
  (reduce (fn [help-string [command-str command-key]]
            (format "%s\n%s ---> %s"
                    help-string
                    command-str
                    command-key))
          ""
          input-string->command))

(def command->delta-row-col
  "Map with information about how a command changes coordinates."
  {:up             [1   0]
   :down           [-1  0]
   :left           [0  -1]
   :right          [0   1]
   :north-east     [1   1]
   :north-west     [1  -1]
   :south-east     [-1  1]
   :south-west     [-1 -1]})

(defn read-command
  "Read a command from user and convert it to a next user move.
  I am inside Emacs now but Vim probably has gotten this right."
  []
  (print "Type a command key (? for help) and Hit Enter> ")
  (flush)
  (or (input-string->command (read-line))
      :invalid-command))

(defn make-user
  "Create a user map given a board to play with.

  State of a game session is kept in the User map instead of associating it
  with the game board. So, multiple users can play on the same board taking
  turns (if ever needed)."
  [{:keys [beg-cell] :as board} initial-points]
  {:current-pos beg-cell
   :visited-cells (conj (cell-neighbors board beg-cell) beg-cell)
   :points initial-points})

(defn next-cell
  "Return users next cell [row col] after taking user-command.
  Returns nil if this step would lead to a cell that is not available, i.e. is
  outside the board."
  [board user user-command]
  (let [current-position (:current-pos user)
        neighboring-cell? (cell-neighbors board current-position)
        enemy-cell? (:enemy-cells board)]
    (->> (command->delta-row-col user-command)
         (map + current-position)
         neighboring-cell?)))

(defn update-points
  "Update points for user based on enemies around at new position."
  [user enemy-count]
  (update user :points #(- % (* enemy-count point-loss-rate-in-combat))))

(defn move-user
  "Given a board and user state, advance the game returning new user state.
  `user-command` is one of the keys in `command->delta-row-col`."
  [board user user-command]
  (let [new-pos (next-cell board user user-command)]
    (cond
      (not new-pos)
      {:status :error
       :info   "Target cell outside board."
       :user user}

      (contains? (:enemy-cells board) new-pos)
      {:status :error
       :info   "Enemy at target position."
       :user user}

      :else
      {:status :ok
       :user (-> user
                 (assoc :current-pos new-pos)
                 (update :visited-cells
                         set/union #{new-pos} (cell-neighbors board new-pos))
                 (update-points (-> (cell-neighbors board new-pos)
                                    (set/intersection (:enemy-cells board))
                                    count)))})))

(defn exec-user-command
  "Take actions based on user-command and return updated user state.
  Returns a map of the form {:user user :board board}.  To make this function
  pure, we can choose to return output string as part of the map and let some
  other function handle printing to the console. That would make testing
  function this easier. Currently, it does the printing as well.

  Check `game.core-rx/game-state->progress-info` for an example."
  [board user user-command]
  {:post [(contains? % :user) (contains? % :board)]}
  (let [state {:board board
               :user user}]
    (case user-command
      :invalid-command
      (do (println "Unknown command key entered")
          state)

      :show-help
      (do (printf "%s%s\n%s"
                  "========================="
                  command-help
                  "=========================")
          state)

      :show-map
      (do (printf "%s\n%s\n%s"
                  "=================Explored Territory==============="
                  (render board user (:visited-cells user))
                  "=================================================")
          state)

      (let [{status :status new-user-state :user info :info}
            (move-user board user user-command)]
        (case status
          :ok    (do (println (:current-pos new-user-state))
                     (assoc state :user new-user-state))
          :error (do (println "Invalid move: " user-command " " info)
                     state))))))

(defn start-game
  "Start the game loop with board for user."
  [{:keys [exit-cell] :as board}
   {:keys [current-pos points] :as user}]
  (println (render board
                   user
                   (conj (cell-neighbors board current-pos)
                         current-pos)))
  (cond
    (not (pos? points))
    (do (println "=========GAME OVER=============\n"
                 ;; User deserves to be told where the exit is.
                 ;; High order functions make it easy.
                 (render board user (constantly true))
                 "=============================="))

    (= exit-cell current-pos)
    (println "You have won!")

    :else
    (let [{:keys [board user]} (exec-user-command board user (read-command))]
      (recur board user))))

(defn -main
  "Starts a gaming session."
  [& args]
  (let [board (gen-board 10 10)
        user  (make-user board 100)]
    (start-game board user)))
