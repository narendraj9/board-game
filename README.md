# A Game 

This project implements a game in Clojure. See [Objective](#objective) for game
description.

There are two implementations here- one using simple functions and the other using
Reactive Extensions (RxClojure). I took this opportunity to learn and implement
something using RxClojure. Both share a lot of code except for the code used
for plumbing functions together.

Information about namespaces:
- `game.board` : defines functions for generating boards, rendering them to
    strings and for finding a path between two cells on a board.
- `game.core` : defines the main execution loop along with user related functions. 
    User related functions could be moved to their own namespace if there was a lot
    of user-related logic. There are kept in this namespace as they are related
    to command definitions in this namespace.
- `game.core-rx` : achieves the same objective as `game.core` using
    RxClojure. Re-uses functions defined both in `game.core`  and `game.board`.

See [Notes](#Notes) for more information.

## [Objective](#objective)

```
Create a very simple text-based adventure game. 
    Create a random game world
        The player starts at a fixed position
        There is an exit randomly placed in the game world
        Objective is to move the player to the exit without the
        player dying
    Whole game world is not visible to the player
        Player can only look around and see their immediate
        surroundings
        But the game has a 'map' feature. Anytime the user requests
        for it, we print an ASCII representation of the currently
        explored game regions.
    Commands to be inputed:
        Movement
        Show map
    There are enemies present in the game world
        Whenever you are in range of an enemy, your health will go
        down by a random factor (10..20 points, for example)
```

## Usage

For executing the first implementation of the game:
    
    $ lein run

For executing the RxClojure implementation:

    $ lein run -m game.core-rx/-main

## [Notes](#Notes)
- Meaning of characters on the board:

| Symbol on Board | Meaning                                           |
|-----------------|---------------------------------------------------|
| #               | A cell that is hidden from the user               |
| e               | A cell that has an enemy                          |
| *               | The exit cell                                     |
| \space          | An empty cell that the user is allowed to move to |

- Printing to the console isn't done with any terminal library, it's a simple
  `print{f,ln}` call. Sometimes the feedback on pressing a keystroke can be
  confusing while playing the game. This happens because the printed text is
  scrolled up by prompt displayed to the user. See a few more lines above to
  check the output of your last game command.
- Keys chosen for navigation are a mixture of Vim navigation keys and keys from
  `picture-mode` in Emacs (north-east/north-west movements, etc.). You can press
  "?" and hit enter during the game to see the key bindings. I am totally loyal
  to Emacs, irrespective of this choice :)
- Board's origin is at bottom left corner, with +x-axis and +y-axis towards the
  right and upwards respectively.
- The user always starts at origin.
- The board always has a path from source cell to the randomly chosen exit cell
  on the board. This path is chosen using DFS search for destination cell from
  the source cell on the board's dual graph. No enemies are placed on this path.
- Enemies are placed randomly on other cells of the board with a probability
  that determines the difficulty level of the game.
- The user loses points on encounter with enemies. She/He has to try to get to
  the exit cell without consuming all of the points. So, the initial points are
  more like what are called lives in most games.

## License

Copyright © 2018 Narendra Joshi

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
