(ns game.core-test
  (:require [clojure.test :refer :all]
            [game.core :refer :all]
            [game.board :refer :all]))

(deftest successful-termination-test
  (testing "Game runs should terminate without exceptions"
    (with-redefs [read-command (fn []
                                 (rand-nth (conj (vals input-string->command)
                                                 :invalid-command)))
                  ;; To speed up execution and these are not thread-safe.
                  enemy-presence-probability 0.5
                  ;; Allowing games to be printed was immensely helpful in
                  ;; understanding edge cases.
                  println (constantly nil)
                  print   (constantly nil)
                  printf  (constantly nil)]
      (doseq [i (range 10)]
        (println "Game: " i)
        (let [row-col-range (vec (range 2 20))
              rows (rand-nth row-col-range)
              cols (rand-nth row-col-range)
              board (gen-board rows cols)
              user (make-user board 100)]
          (is (thrown-with-msg? Exception
                                #"SUCCESS"
                                (do (start-game board user)
                                    ;; This Exception won't be thrown if
                                    ;; start-game throws an Exception.
                                    (throw (Exception. "SUCCESS"))))))))))
